# tio-webpack
传统前端的webpack，专门为传统前端开发的编译、压缩、打包工具，极大极大提升开发效率
![输入图片说明](https://gitee.com/tywo45/tio-webpack/raw/master/doc/1.png "在这里输入图片标题")

## 使用场景
- html代码段复用
常见的开发场景如header.html + xxx.html + foot.html ==> allXxx.html

- html,css,js代码编译压缩合并

- 为还处在开发期的html,css,js提供实时运行效果

## 效果预览
以下截图，左侧为开发人员视图，右侧为实际运行后的视图

![输入图片说明](https://gitee.com/tywo45/tio-webpack/raw/master/doc/2.png "在这里输入图片标题")

![输入图片说明](https://gitee.com/tywo45/tio-webpack/raw/master/doc/3.png "在这里输入图片标题")

![输入图片说明](https://gitee.com/tywo45/tio-webpack/raw/master/doc/3-3.png "在这里输入图片标题")

![输入图片说明](https://gitee.com/tywo45/tio-webpack/raw/master/doc/4.png "在这里输入图片标题")

![输入图片说明](https://gitee.com/tywo45/tio-webpack/raw/master/doc/4-4.png "在这里输入图片标题")

![输入图片说明](https://gitee.com/tywo45/tio-webpack/raw/master/doc/5.png "在这里输入图片标题")

![输入图片说明](https://gitee.com/tywo45/tio-webpack/raw/master/doc/5-5.png "在这里输入图片标题")


## 使用方法
- 下载代码
[https://gitee.com/tywo45/tio-webpack](https://gitee.com/tywo45/tio-webpack)

-  找到release/config/app-env.properties
    把里面的http.page值配成你的网页目录
    把build.to值配成你的打包后的目录

- 运行


    1、双击release/run.bat

    2、访问[http://127.0.0.1:10150](http://127.0.0.1:10150)，便可访问你的网页了

- 打包


    1、双击release/dist.bat

    2、生成的文件会在build.to指定的目录中

## 一分钟掌语法
如果你已经会使用freemarker，这一分钟都可以省掉啦，因为tio-webpack的语法和freemarker一模一样的，如果你不会freemarker，只需要掌握下面两个指令即可完成相当不错的功能
### 1. include指令
```
<#include "/js/app/a.js">
```

### 2. if指令
```
<#if console.log == true >
    var log = console.log.bind(console);
<#else>
    var log = function () { }; 
</#if>
```


#### 大家先可以关注一下该项目的母项目：[t-io不仅仅是百万级即时通讯框架](https://gitee.com/tywo45/t-io)，等有空了，给大家整个样例工程，这样学起来快