@echo off
setlocal & pushd

set APP_ENTRY=org.tio.webpack.app.TioWebpackBuildStarter

set BASE=%~dp0
set CP=%BASE%\config;%BASE%\lib\*
java -Xverify:none -XX:+HeapDumpOnOutOfMemoryError -Xrunjdwp:transport=dt_socket,address=9999,suspend=n,server=y -cp "%CP%" %APP_ENTRY% dist.json
endlocal & popd
