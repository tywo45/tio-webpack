@echo off
setlocal & pushd

set APP_ENTRY=org.tio.webpack.app.TioWebpackWebServerStarter

set BASE=%~dp0
set CP=%BASE%\config;%BASE%\lib\*
java -Xverify:none -XX:+HeapDumpOnOutOfMemoryError -Xrunjdwp:transport=dt_socket,address=8888,suspend=n,server=y -cp "%CP%" %APP_ENTRY% run.json
endlocal & popd
