package org.tio.webpack.app.http;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tio.http.common.HttpRequest;
import org.tio.http.server.view.ModelGenerator;

/**
 * @author tanyaowu 
 * 2017年11月15日 下午1:14:20
 */
public class TioWebpackModelGenerator implements ModelGenerator {
	private static Logger log = LoggerFactory.getLogger(TioWebpackModelGenerator.class);

//	public static TioWebpackModelGenerator me = new TioWebpackModelGenerator();

	private Map mapModel;

	/**
	 * 
	 * @author tanyaowu
	 */
	public TioWebpackModelGenerator(Map mapModel) {
		this.mapModel = mapModel;
	}

	/** 
	 * @param request
	 * @return
	 * @author tanyaowu
	 */
	@Override
	public Object generate(HttpRequest request) {
		return mapModel;
	}

	/**
	 * @param args
	 * @author tanyaowu
	 */
	public static void main(String[] args) {

	}
}
