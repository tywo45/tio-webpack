package org.tio.webpack.app;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tio.flash.policy.server.FlashPolicyServerStarter;
import org.tio.utils.json.Json;
import org.tio.webpack.app.http.HttpServerInit;
import org.tio.webpack.app.http.TioWebpackModelGenerator;
import org.tio.webpack.init.PropInit;
import org.tio.webpack.model.Root;

import com.jfinal.kit.PropKit;

/**
 * @author tanyaowu
 * 2017年8月7日 上午10:58:03
 */
public class TioWebpackWebServerStarter {
	private static Logger log = LoggerFactory.getLogger(TioWebpackWebServerStarter.class);
	public static Root model = null;
	public static Map mapModel = null;


	/**
	 * @param args
	 * @author tanyaowu
	 * @throws IOException
	 */
	public static void main(String[] args) throws Exception {
		try {
			
			model = null;
			String modelFilePath;
			if (args != null && args.length >0) {
				modelFilePath = args[0];
			} else {
				modelFilePath = "run.json";
			}
			
			String modelAbsFile = com.xiaoleilu.hutool.io.FileUtil.getAbsolutePath("classpath:" + modelFilePath);
			File modelFile = new File(modelAbsFile);
			log.info("modelAbsFile:{}", modelAbsFile);
			String content = com.xiaoleilu.hutool.io.FileUtil.readString(modelFile, "utf-8");
			model = Json.toBean(content, Root.class);
			mapModel = Json.toBean(content, Map.class);
			
			PropInit.init();
			//
			
			TioWebpackModelGenerator tioWebpackModelGenerator = new TioWebpackModelGenerator(mapModel);

			HttpServerInit.init(model, tioWebpackModelGenerator);
			
			if (PropKit.getBoolean("start.843", false)) {
				FlashPolicyServerStarter.start(null, null);
			}			
		} catch (Throwable e) {
			log.error(e.toString(), e);
			System.exit(1);
		}
	}

	/**
	 *
	 * @author tanyaowu
	 */
	public TioWebpackWebServerStarter() {
	}
}
